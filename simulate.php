<!DOCTYPE html>
<html>
<head>
	<title>Vue</title>
	<link rel="stylesheet" href="boot/css/bootstrap.min.css">
	<link rel="stylesheet" href="boot/css/style.css">
	<script src="boot/js/jquery.min.js"></script>
	<script src="boot/js/popper.min.js"></script>
	<script src="boot/js/bootstrap.min.js"></script>
	<link href="fontawesome/css/all.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	<!-- Brand -->
	<img src="images/logo.png" alt="Logo" style="width:40px;">
	<a class="navbar-brand" href="#">Sony Sugar</a>

	<!-- Links -->
	<ul class="navbar-nav mr-auto">
		<li class="nav-item">
			<a class="nav-link " href="index.php">Home</a>
		</li>
		<li class="nav-item">
			<a class="nav-link active" href="simulate.php">Simulate</a>
		</li>
		<!-- Dropdown -->
	</ul>
	<ul class="navbar-nav ml-auto">
		<form class="form-inline">
			<input class="form-control form-control-sm" type="text" placeholder="Search" id="myInput">
		</form>
	</ul>
</nav>
<div class="container">
	<?php
	require_once 'access.php';
	$url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate';
	$token = getAccessToken();
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$token)); //setting custom header


	$curl_post_data = array(
		//Fill in the request parameters with valid values
		'ShortCode' => '600314',
		'CommandID' => 'CustomerPayBillOnline',
		'Amount' => '235',
		'Msisdn' => '0712083128',
		'BillRefNumber' => '0712083128'
	);

	$data_string = json_encode($curl_post_data);

	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

	$curl_response = curl_exec($curl);
	print_r($curl_response);

	echo $curl_response;
	?>


	<div class="container">
		<h2>Use the following URL for Postman Simulation</h2>
		<div class="alert alert-success">
			<p>http://product.test/callBack.php</p>
		</div>

	</div>
</div>
</body>
</html>