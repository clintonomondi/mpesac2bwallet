<?php
/**
 * Created by PhpStorm.
 * User: MadProgrammer
 * Date: 3/8/2019
 * Time: 5:17 PM
 */
//include "connection.php";

require_once 'connection.php';

$out = ['error' => false];
$crud = 'read';

if (isset($_GET['crud'])) {
	$crud = $_GET['crud'];
}
if ($crud = 'read') {
	$sql = "select * from customer";
	$query = $conn->query($sql);
	$members = [];
	while ($row = $query->fetch_array()) {
		array_push($members, $row);
	}

	$out['members'] = $members;
}


$conn->close();

header("Content-type: application/json");
echo json_encode($out);
die();
