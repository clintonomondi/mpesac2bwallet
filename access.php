<?php

function getAccessToken() {
	$url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	$credentials = base64_encode('wAgXqCMgOTRM2LRGb3opf789svNeKvZQ:IGnbYjWXCfMUCiSr');
	curl_setopt($curl, CURLOPT_HTTPHEADER, ['Authorization: Basic ' . $credentials,
		'Content-Type: application/json',
		'Accept: application/json'
	]); //setting a custom header
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response = curl_exec($curl);
	curl_close($curl);
	$json = json_decode($curl_response, true);
    return $json['access_token'];
}