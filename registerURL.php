<?php
require_once 'access.php';
function registerURL() {
	$url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl';
	$token = getAccessToken();
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json', 'Authorization:Bearer '.$token]); //setting custom header

	$curl_post_data = [
		//Fill in the request parameters with valid values
		'ShortCode' => '600314',
		'ResponseType' => 'json',
		'ConfirmationURL' => 'https://7c484806.ngrok.io/callBack.php',
		'ValidationURL' => 'https://7c484806.ngrok.io/callBack.php'
	];
	$data_string = json_encode($curl_post_data);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

	$curl_response = curl_exec($curl);
	curl_close($curl);


	return $curl_response;
}

echo  registerURL();
