<?php

?>
<!DOCTYPE html>
<html>
<head>
    <title>Vue</title>
    <link rel="stylesheet" href="boot/css/bootstrap.min.css">
    <link rel="stylesheet" href="boot/css/style.css">
    <script src="boot/js/jquery.min.js"></script>
    <script src="boot/js/popper.min.js"></script>
    <script src="boot/js/bootstrap.min.js"></script>
    <link href="fontawesome/css/all.min.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand -->
    <img src="images/logo.png" alt="Logo" style="width:40px;">
    <a class="navbar-brand" href="#">Sony Sugar</a>

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link active" href="index.php">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="simulate.php">Simulate</a>
        </li>
        <!-- Dropdown -->
    </ul>
    <ul class="navbar-nav ml-auto">
        <form class="form-inline">
            <input class="form-control form-control-sm" type="text" placeholder="Search" id="myInput">
        </form>
    </ul>
</nav>
<div class="container">
    <div id="members">
            <table class="table  table-striped">
                <thead>
                <th>ID</th>
                <th>Phone no.</th>
                <th>Amount</th>
                <th>Mpesa ID</th>
                <th>Time</th>
                <th>Action</th>
                </thead>
                <tbody id="myTable">
                <tr v-for="member in members">
                    <td>{{ member.id }}</td>
                    <td>{{ member.phone }}</td>
                    <td>{{ member.amount }}</td>
                    <td>{{ member.code }}</td>
                    <td>{{ member.time }}</td>
                    <td>
                        <a class="btn btn-success btn-sm fa fa-edit"></a>
                        <a class="btn btn-danger btn-sm fa fa-cut" @click='deleteRecord(member.id)'></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="js/vue.js"></script>
<script src="js/axios.min.js"></script>
<script src="js/app.js"></script>
<script>
	$(document).ready(function(){
		$("#myInput").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#myTable tr").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});
	});
</script>
</body>
</html>